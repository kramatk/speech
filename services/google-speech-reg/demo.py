from __future__ import print_function

from twisted.internet import reactor, protocol
import json

# a client protocol

class EchoClient(protocol.Protocol):
    def connectionMade(self):
        self.transport.write(json.dumps({
            "filename": "resources/test1.m4a"
        }))
    
    def dataReceived(self, data):
        print("Server said:", data)
        self.transport.loseConnection()
    
    def connectionLost(self, reason):
        pass
        # print("connection lost")

class EchoFactory(protocol.ClientFactory):
    protocol = EchoClient

    def clientConnectionFailed(self, connector, reason):
        print("Connection failed - goodbye!")
        reactor.stop()
    
    def clientConnectionLost(self, connector, reason):
        print("Connection lost - goodbye!")
        reactor.stop()


# this connects the protocol to a server running on port 8000
def main():
    f = EchoFactory()
    reactor.connectTCP("localhost", 8000, f)
    reactor.run()

# this only runs if the module was *not* imported
if __name__ == '__main__':
    main()


# from AudioFile import AudioFile
# from Speech import Speech
# import json
# if __name__ == "__main__":
#     print "AA"

#     audio = AudioFile("resources/test1.m4a")
#     service = Speech()

#     print json.dumps(service.getText(audio))
