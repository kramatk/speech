import io
import os
from Converter import Converter

class AudioFile:
    converter = Converter()

    def __init__(self, filename):
        self.setupPath(filename)
        
        if self.ext not in [".mp3", ".wav", ".m4a", ".raw"]:
            raise Exception("Not support file type")

        if self.ext == ".mp3":
            newPath, audioFile = AudioFile.converter.mp3ToWav(self)
            self.setupPath(newPath)
        elif self.ext == ".m4a":
            newPath, audioFile = AudioFile.converter.m4aToWav(self)
            self.setupPath(newPath)

        self.frame_rate = audioFile.frame_rate
        
    def setupPath(self, filename):
        self.filename = os.path.basename(filename)
        self.ext = os.path.splitext(filename)[-1].lower()
        self.filepath = os.path.join(os.path.dirname(__file__), '', filename)

    def __del__(self):
        pass