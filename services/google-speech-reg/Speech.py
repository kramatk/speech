# Docs: https://googlecloudplatform.github.io/google-cloud-python/latest/speech/index.html

from google.cloud import speech
from google.cloud.speech import enums
from google.cloud.speech import types
import os, io

class Speech:
    def __init__(self):
        self.client = speech.SpeechClient()

    def getText(self, inputAudio):

        # Instantiates a client
        

        # The name of the audio file to transcribe
        file_name = inputAudio.filepath

        # Loads the audio into memory
        with io.open(file_name, 'rb') as audio_file:
            content = audio_file.read()
            audio = types.RecognitionAudio(content=content)

        config = types.RecognitionConfig(
            encoding=enums.RecognitionConfig.AudioEncoding.LINEAR16,
            sample_rate_hertz=inputAudio.frame_rate,
            language_code='en-US')

        # Detects speech in the audio file
        response = self.client.recognize(config, audio)

        text = []
        for result in response.results:
            text.append({
                "transcript": result.alternatives[0].transcript,
                "confidence": result.alternatives[0].confidence
            })

        return text

    def __del__(self):
        pass