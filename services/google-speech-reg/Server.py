from AudioFile import AudioFile
from Speech import Speech
import json

from twisted.internet import reactor, protocol

class Echo(protocol.Protocol):

    def dataReceived(self, rawData):
        
        print rawData
        try:
            data = json.loads(rawData)
            audio = AudioFile(data["filename"])
            service = Speech()
            self.transport.write(json.dumps(service.getText(audio)))
        except Exception:
            self.transport.write(rawData)
            


def main():
    """This runs the protocol on port 8000"""
    factory = protocol.ServerFactory()
    factory.protocol = Echo
    reactor.listenTCP(8000,factory)
    reactor.run()

# this only runs if the module was *not* imported
if __name__ == '__main__':
    main()
