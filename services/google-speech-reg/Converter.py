from pydub import AudioSegment
import io
import os

class Converter:
    def __init__(self):
        pass

    def outputPath(self, pathname):
        # use $$## to guarantee that it will be affected only the end of string
        # TODO: it is just a lazy appoarch
        return (pathname+"$$##").replace(".m4a$$##", '_output.wav')

    def mp3ToWav(self, autio):
        audioFile = AudioSegment.from_file(autio.filepath, format="mp3")
        newPath = self.outputPath(autio.filepath)
        audioFile.set_channels(1).export(newPath, format="wav")
        return newPath, audioFile
        

    def m4aToWav(self, autio):
        audioFile = AudioSegment.from_file(autio.filepath, format="m4a")
        newPath = self.outputPath(autio.filepath)
        audioFile.set_channels(1).export(newPath, format="wav")
        return newPath, audioFile

    def __del__(self):
        pass