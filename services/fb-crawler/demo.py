from selenium import webdriver
from selenium.webdriver.chrome.options import Options
import os
from selenium.webdriver.common.keys import Keys

chrome_options = Options()
chrome_options.add_argument("--headless")
chrome_options.add_argument("--window-size=1920x1080")

# download the chrome driver from https://sites.google.com/a/chromium.org/chromedriver/downloads and put it in the
# current directory
chrome_driver = os.getcwd() +"\\chromedriver.exe"

# go to Google and click the I'm Feeling Lucky button
driver = webdriver.Chrome(chrome_options=chrome_options, executable_path=chrome_driver)
driver.get("https://www.facebook.com")
# lucky_button = driver.find_element_by_css_selector("#email")
email_input = driver.find_element_by_id("email")
email_input.send_keys('**email***')

pass_input = driver.find_element_by_id("pass")
pass_input.send_keys('**pass**')

driver.find_element_by_id('login_form').submit()
# capture the screen
driver.get_screenshot_as_file("capture.png")