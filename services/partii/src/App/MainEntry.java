package App;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import th.or.nectec.partii.core.Partii.PartiiConfig;
import th.or.nectec.partii.core.Partii.PartiiCoreKit;
import th.or.nectec.partii.core.Partii.ResultCallBack;
import th.or.nectec.partii.core.Partii.StatusCodes;

public class MainEntry implements ResultCallBack {
	PartiiCoreKit partycore;
	PartiiConfig conf;
	RandomAccessFile file;
	
	String apikey = "804008544c6669a0ad77f72f8df9b639";
	String ip = "127.0.0.1";
	String id = "PC-MAXILE";
	int port = 9876;
	int samplerate = 16000;
	String filename = "test.wav";
	double chunk_length_secs = 0.064;
	
	
	public static void main(String args[]) {
		new MainEntry().start();
	}
	
	public void start() {
		conf = new PartiiConfig(ip, port, id, apikey, samplerate);
		
		partycore = new PartiiCoreKit(conf);
		partycore.enableTimeout(true);
		partycore.setTimeWaitingForResponse(60 * 1000); // 1 minute

		startStream();
	}
	
	public void startStream() {
		try {
			file = new RandomAccessFile(filename, "r");
		} catch (FileNotFoundException e1) {
			e1.printStackTrace();
		}
		partycore.prepareStream(this);
		Thread t = new Thread() {
			@Override
			public void run() {
				int length = 0;
				int buffer_len = (int)(conf.getSamplerate() * chunk_length_secs);
				
				byte[] buffer = new byte[buffer_len];
				try {
					while ((length = file.read(buffer)) > 0) {
						partycore.addByteStream(buffer, 0, length);
						
						double len_per_sec = samplerate * (16 / 8);
						double delay_time = 1000 / ( len_per_sec / buffer_len);
						try {
							Thread.sleep((long) delay_time);
						} catch (InterruptedException e) {
							e.printStackTrace();
						}
					}
					partycore.endOfStream();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		};
		t.start();
	}
	
	@Override
	public void onResultCallBack(String result) {
		JSONParser parser = new JSONParser();
		JSONObject json;
		try {
			json = (JSONObject) parser.parse(result);
			
			if (json.get(StatusCodes.TAG).equals(StatusCodes.SUCCESS)) {
				System.out.println("Result = " + partycore.decodeStr(json.get("result").toString()) + ", utt = " + json.get("utt").toString());
			} else if (json.get(StatusCodes.TAG).equals(StatusCodes.EOS)) {
				System.out.println("End of utterance.");
				partycore.stopService();
			} else {
				System.out.println("Result " + json.toJSONString());
				System.exit(-1);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Override
	public void onFinishSendingPackage() {
	}
}
